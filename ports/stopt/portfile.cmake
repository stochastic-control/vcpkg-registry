vcpkg_buildpath_length_warning(37)

# get back tar.gz of tag
vcpkg_from_gitlab(
    GITLAB_URL https://gitlab.com
    OUT_SOURCE_PATH SOURCE_PATH 
    REPO stochastic-control/StOpt
    REF  v5.12
    SHA512  9518c8e89bfe1612737271340c39eb24dbc9e3e6a040ad2ba998ae27840f9461378f3af75432738a9e828712719231a0508748904da81be71a86dbf702eff021
    HEAD_REF master  
)

vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    PREFER_NINJA
    OPTIONS
    -DBUILD_SDDP=ON
    -DBUILD_DPCUTS=ON
    -DBUILD_PYTHON=OFF
    -DBUILD_TEST=OFF    
)

vcpkg_install_cmake()
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/doc"  "${CURRENT_PACKAGES_DIR}/debug/doc")
file(INSTALL ${SOURCE_PATH}/LICENCE.txt DESTINATION ${CURRENT_PACKAGES_DIR}/share/${PORT} RENAME copyright)

